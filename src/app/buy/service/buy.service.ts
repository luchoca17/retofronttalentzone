import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ProductsInterface } from 'src/app/products/interface/products-interface';
import { BuyInterface } from '../interface/buy-interface';

@Injectable({
  providedIn: 'root',
})
export class BuyService {
  private apiUrl: string = ' http://localhost:8080/product';
  private body: BuyInterface = {
    products: [],
  };
  productsBuyed: ProductsInterface[] = [];

  constructor(private _httpClient: HttpClient) {}

  createBuy(): Observable<BuyInterface> {
    const url = `${this.apiUrl}/createBuy`;
    return this._httpClient.post<BuyInterface>(url, this.body);
  }

  listBougthProducts(): ProductsInterface[] {
    return this.productsBuyed;
  }

  addProduct(product: ProductsInterface) {
    this.productsBuyed.push(product);
  }
}
