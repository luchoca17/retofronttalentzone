import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyComponent } from './buy.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [BuyComponent],
  imports: [CommonModule, MaterialModule, RouterModule],
})
export class BuyModule {}
