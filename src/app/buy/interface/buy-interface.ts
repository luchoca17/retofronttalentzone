import { ProductsInterface } from 'src/app/products/interface/products-interface';

export interface BuyInterface {
  clientName?: string;
  products: ProductsInterface[];
}
