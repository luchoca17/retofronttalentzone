import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductsInterface } from '../products/interface/products-interface';
import { BuyService } from './service/buy.service';

export interface tabla {
  name: string;
  inInventory: number;
  enabled: boolean;
  min: number;
  max: number;
}

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css'],
})
export class BuyComponent implements OnInit, AfterViewInit {
  productosComprados: ProductsInterface[] = [];
  displayedColumns: string[] = ['index', 'name', 'inInventory', 'enabled'];
  dataSource = new MatTableDataSource<tabla>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private _buyService: BuyService) {}

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.shoppingCart();
    console.log('carrito compras', this.productosComprados);
    this.dataSource.data = this.productosComprados;
  }

  shoppingCart() {
    this.productosComprados = this._buyService.listBougthProducts();
  }
}
