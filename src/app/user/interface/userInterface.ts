import { BuyInterface } from 'src/app/buy/interface/buy-interface';

export interface UserInterface {
  userName: string;
  email: string;
  idType: string;
  buys: BuyInterface[];
}
