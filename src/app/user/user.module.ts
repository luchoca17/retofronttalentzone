import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [UserComponent],
  imports: [RouterModule, CommonModule, MaterialModule],
})
export class UserModule {}
