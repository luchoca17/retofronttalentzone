import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPoroductsComponent } from './add-poroducts.component';

describe('AddPoroductsComponent', () => {
  let component: AddPoroductsComponent;
  let fixture: ComponentFixture<AddPoroductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPoroductsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddPoroductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
