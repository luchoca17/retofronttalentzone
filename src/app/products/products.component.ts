import { Component, OnInit } from '@angular/core';
import { BuyService } from '../buy/service/buy.service';
import { ProductsInterface } from './interface/products-interface';
import { ProductsService } from './service/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  products: ProductsInterface[] = [];

  constructor(
    private _productsService: ProductsService,
    private _buyService: BuyService
  ) {
    JSON.parse(localStorage.getItem('productosComprados') || '{}');
  }

  ngOnInit() {
    this.listarProducts();
  }

  listarProducts() {
    this._productsService
      .findAllProducts()
      .subscribe((prod: ProductsInterface[]) => {
        this.products = prod;
        console.log('estoy en listar productos');
        console.log(prod);
        console.log(this.products);
      });
  }

  comprar(prod: ProductsInterface) {
    this._buyService.addProduct(prod);
    localStorage.setItem('productosComprados', JSON.stringify(prod));
  }
}
