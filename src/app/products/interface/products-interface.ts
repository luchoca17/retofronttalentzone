export interface ProductsInterface {
  name: string;
  inInventory: number;
  enabled: boolean;
  min: number;
  max: number;
}
