import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPoroductsComponent } from './add-poroducts/add-poroducts.component';
import { ProductsComponent } from './products.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductsComponent,
    children: [
      {
        path: 'products/addProd',
        component: AddPoroductsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}
