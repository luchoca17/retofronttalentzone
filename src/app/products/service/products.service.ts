import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsInterface } from '../interface/products-interface';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private apiUrl: string = ' http://localhost:8080/product';
  private body: ProductsInterface = {
    name: 'string',
    inInventory: 500,
    enabled: true,
    min: 1,
    max: 12,
  };
  productos!: ProductsInterface[];

  constructor(private _httpClient: HttpClient) {}

  findAllProducts(): Observable<ProductsInterface[]> {
    const url = `${this.apiUrl}/findAllProducts`;
    console.log(url);
    return this._httpClient.get<ProductsInterface[]>(url);
  }

  createProducts(): Observable<ProductsInterface> {
    const url = `${this.apiUrl}/createProducts`;
    return this._httpClient.post<ProductsInterface>(url, this.body);
  }
}
