import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { AddPoroductsComponent } from './add-poroducts/add-poroducts.component';
import { ProductsRoutingModule } from './products-routing.module';

@NgModule({
  declarations: [ProductsComponent, AddPoroductsComponent],
  exports: [ProductsComponent, AddPoroductsComponent],
  imports: [CommonModule, MaterialModule, RouterModule, ProductsRoutingModule],
})
export class ProductsModule {}
